#!/bin/sh

# Copyright 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="e2fsprogs"
VERSION=${VERSION:-"1.42.13"}
LINK=${LINK:-"http://downloads.sourceforge.net/${NAME}/${NAME}-${VERSION}.tar.gz"}
NUMJOBS=${NUMJOBS:-" -j7 "}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-e2fsprogs
RELEASEDIR=${CWD}/..

# GET THE SOURCE CODE
for src in $(echo $LINK ); do
	(
	cd $CWD
	wget -c --no-check-certificate $src
	)
done

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf e2fsprogs-$VERSION
tar xvf $CWD/e2fsprogs-$VERSION.tar.?z* || exit 1
cd e2fsprogs-$VERSION || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# Disable libblkid and libuuid, as we'll be using the ones in u-l-ng
CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix= \
  --libdir=/lib${LIBDIRSUFFIX} \
  --bindir=/usr/bin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/e2fsprogs-$VERSION \
  --enable-elf-shlibs \
  --disable-libblkid \
  --disable-libuuid \
  --disable-uuidd \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1
make install-libs DESTDIR=$PKG || exit 1

# I guess Ted would rather not have this included, so we won't.
# ( cd misc 
#   make findsuper
#   cat findsuper > $PKG/sbin/findsuper
#   chmod 0755 $PKG/sbin/findsuper )

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Don't clobber an existing config file
mv $PKG/etc/mke2fs.conf $PKG/etc/mke2fs.conf.new

# Fix up package:
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}
mv $PKG/lib${LIBDIRSUFFIX}/pkgconfig $PKG/lib${LIBDIRSUFFIX}/*.so \
  $PKG/usr/lib${LIBDIRSUFFIX}
( cd $PKG/usr/lib${LIBDIRSUFFIX}
  for i in *.so ; do 
    ln -sf /lib${LIBDIRSUFFIX}/$(readlink $i) $i ; 
  done
)
# findfs is intentionally left out here - we use the one in util-linux-ng
( cd $PKG/sbin
  rm -f \
    mkfs.ext2 mkfs.ext3 mkfs.ext4 mkfs.ext4dev \
    fsck.ext2 fsck.ext3 fsck.ext4dev e2label findfs
  ln -sf mke2fs mkfs.ext2
  ln -sf mke2fs mkfs.ext3
  ln -sf mke2fs mkfs.ext4
  ln -sf mke2fs mkfs.ext4dev
  ln -sf tune2fs e2label
  cat << EOF > fsck.ext2
#!/bin/sh
exec /sbin/e2fsck -C 0 \$*
EOF
  chmod 0755 fsck.ext2
  # Why wont symlinks work here?  --RW
  # Because $0 will always be "fsck.ext2" in that case.  --PJV
  cp -a fsck.ext2 fsck.ext3
  cp -a fsck.ext2 fsck.ext4
  cp -a fsck.ext2 fsck.ext4dev
)
( cd $PKG/usr/man/man3
  rm -f uuid_generate_random.3 uuid_generate_time.3
  ln -sf uuid_generate.3 uuid_generate_random.3
  ln -sf uuid_generate.3 uuid_generate_time.3 
)
( cd $PKG/usr/man/man8
  rm -f fsck.ext2.8 fsck.ext3.8 mkfs.ext2.8 mkfs.ext3.8 \
    mkfs.ext4.8 mkfs.ext4dev.8
  ln -sf e2fsck.8 fsck.ext2.8
  ln -sf e2fsck.8 fsck.ext3.8
  ln -sf e2fsck.8 fsck.ext4.8
  ln -sf e2fsck.8 fsck.ext4dev.8
  ln -sf mke2fs.8 mkfs.ext2.8
  ln -sf mke2fs.8 mkfs.ext3.8
  ln -sf mke2fs.8 mkfs.ext4.8
  ln -sf mke2fs.8 mkfs.ext4dev.8
)

# Compress and link manpages
( cd $PKG/usr/man
  find . -type f -exec gzip -9 {} \;
  for i in $( find . -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
)

mkdir -p $PKG/usr/doc/e2fsprogs-$VERSION
cp -a \
  COPYING* INSTALL INSTALL.elfbin README* RELEASE-NOTES SHLIBS \
  $PKG/usr/doc/e2fsprogs-$VERSION
chmod 644 $PKG/usr/doc/e2fsprogs-$VERSION/*

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $RELEASEDIR/slack-desc


cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
/sbin/makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
fi
