#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="gdb"
VERSION=${VERSION:-"7.9.1"}
LINK=${LINK:-"http://ftp.gnu.org/gnu/${NAME}/${NAME}-${VERSION}.tar.xz"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

NUMJOBS=${NUMJOBS:-" -j7 "}
PKGNAM=${NAME}

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-gdb
RELEASEDIR=${RELEASEDIR:-"$CWD/.."}

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

# download source dir
	(
	cd $CWD
	for link in $(echo $LINK); do
		wget -c --no-check-certificate $link
	done
	)

rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf gdb-$VERSION
tar xvf $CWD/gdb-$VERSION.tar.?z* || exit 1
cd gdb-$VERSION || exit 1

#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
for i in $CWD/patches/*;do
  patch -p1 <$i
  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
done
#-----------------------------------------------------

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --with-python \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

( cd readline ; make || exit 1)

make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

# None of this stuff has ever been included in this package:
rm -f $PKG/usr/lib${LIBDIRSUFFIX}/{libbfd*,libiberty*,libopcodes*}
rmdir $PKG/usr/lib${LIBDIRSUFFIX} 2> /dev/null
rm -f $PKG/usr/info/{annotate*,bfd*,configure*,standards*}
rm -rf $PKG/usr/include

# Use the -tui option if you want this.
# Including a whole extra copy of the gdb binary is obnoxious:
rm -f $PKG/usr/bin/gdbtui $PKG/usr/man/man1/gdbtui.1*

mkdir -p $PKG/usr/doc/gdb-$VERSION/gdb
cp -a COPYING* README $PKG/usr/doc/gdb-$VERSION
cd gdb
cp -a NEWS README $PKG/usr/doc/gdb-$VERSION/gdb
cp -a gdbserver/README $PKG/usr/doc/gdb-$VERSION/README.gdbserver
find $PKG/usr/doc/gdb-$VERSION -type f -exec chmod 644 {} \;

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Compress info files, if any:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

# Build the package:
cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $RELEASEDIR/${NAME}-$VERSION-$ARCH-$BUILD.txz
fi
