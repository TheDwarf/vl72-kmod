#!/bin/sh

# Slackware Package Build Script for Dirac
# Home Page http://www.diracvideo.org/

# Copyright (c) 2008-2010, Nishant Limbachia, Hoffman Estates, IL, USA (nishant _AT_ mnspace _DOT_ net)
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of script must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="dirac"
VERSION=${VERSION:-"1.0.2"}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"doxygen"}
LINK=${LINK:-"http://diracvideo.org/download/dirac-research/${NAME}-${VERSION}.tar.gz"}


# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) ARCH=$( uname -m ) ;;
  esac
fi
PRGNAM=$NAME
CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-${CWD}/..}


if [ "$NORUN" != 1 ]; then

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

# Download the source code
for link in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $link
	)
done
set -e 	# Exit on most errors

rm -fr $TMP/$PRGNAM-$VERSION $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
tar xvf $CWD/$PRGNAM-$VERSION.tar.gz
cd $PRGNAM-$VERSION
chown -R root.root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Apply some upstream patches
patch -p1 < $CWD/encoder.patch --verbose
patch -p1 < $CWD/gcc45.patch
patch -p1 < $CWD/no-werror.patch

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --disable-static \
  --build=$ARCH-slackware-linux || exit 1

make || exit 1
make install \
  htmldir=/usr/doc/$PRGNAM-$VERSION/html \
  DESTDIR=$PKG || exit 1

### strip binaries
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

cp -a AUTHORS ChangeLog COPYING INSTALL \
  NEWS README TODO $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $OUTPUT/slack-desc

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD.${PKGTYPE:-txz}
fi
